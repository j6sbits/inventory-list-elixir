defmodule InventoryListElixirWeb.Router do
  use InventoryListElixirWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", InventoryListElixirWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api/v1", InventoryListElixirWeb do
    pipe_through :api

    get "/inventory", InventoryController, :list
    get "/inventory/:product_id", InventoryController, :show
  end
end
