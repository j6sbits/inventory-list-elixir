defmodule InventoryListElixirWeb.InventoryController do
  use InventoryListElixirWeb, :controller

  def show(conn, _params) do
    page = %{product: "Arroz Roa 500gr", lote: "001"}
    render(conn, "show.json", page: page)
  end 

  def list(conn, _params) do
    pages = Mongo.find(:mongo, "inventories", %{})
    render(conn, "list.json", pages: pages)
  end
end