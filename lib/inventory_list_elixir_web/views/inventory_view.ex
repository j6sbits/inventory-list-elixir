defmodule InventoryListElixirWeb.InventoryView do
  use InventoryListElixirWeb, :view

  def render("show.json", %{page: page}) do
    %{data: render_one(page, __MODULE__, "inventory.json", as: :page)}
  end

  def render("list.json", %{pages: pages}) do
    %{data: render_many(pages, __MODULE__, "inventory.json", as: :page)}
  end

  def render("inventory.json", %{page: page}) do
    %{product: page["product"], lote: page["lote"]}
  end

end