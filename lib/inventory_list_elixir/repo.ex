defmodule InventoryListElixir.Repo do
  use Ecto.Repo,
    otp_app: :inventory_list_elixir,
    adapter: Ecto.Adapters.MyXQL
end
