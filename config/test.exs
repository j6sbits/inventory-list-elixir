use Mix.Config

# Configure your database
config :inventory_list_elixir, InventoryListElixir.Repo,
  username: "root",
  password: "",
  database: "inventory_list_elixir_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :inventory_list_elixir, InventoryListElixirWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
